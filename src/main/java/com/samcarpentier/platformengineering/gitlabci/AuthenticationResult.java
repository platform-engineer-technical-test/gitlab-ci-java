package com.samcarpentier.platformengineering.gitlabci;

public enum AuthenticationResult {
  SUCCESS, FAILURE
}
