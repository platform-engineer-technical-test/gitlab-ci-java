package com.samcarpentier.platformengineering.gitlabci;

import java.util.logging.Logger;

public class ExtremelyComplexBusinessLogic {

  private static final Logger logger = Logger.getLogger(ExtremelyComplexBusinessLogic.class.getName());
  private final AuthenticationService authenticationService;

  public ExtremelyComplexBusinessLogic(AuthenticationService authenticationService) {
    this.authenticationService = authenticationService;
  }

  public void authenticateCustomer(String username, String password) {
    AuthenticationResult result = authenticationService.login(username, password);
    if (result == AuthenticationResult.SUCCESS) {
      logger.info("Authentication successful!");
    } else {
      throw new RuntimeException("Auth failed!");
    }
  }
}
