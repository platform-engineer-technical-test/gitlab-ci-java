package com.samcarpentier.platformengineering.gitlabci;

import java.util.logging.Logger;

public class ApplicationMain implements Runnable {

  private static final Logger logger = Logger.getLogger(ApplicationMain.class.getName());
  private static final String USERNAME = "user";
  private static final String PASSWORD = "pass";

  public static void main(String[] args) {
    ApplicationMain main = new ApplicationMain();
    main.run();
  }

  @Override
  public void run() {
    logger.info("Application is starting...");

    AuthenticationService authenticationService = new AuthenticationServiceImpl();
    ExtremelyComplexBusinessLogic logic = new ExtremelyComplexBusinessLogic(authenticationService);
    logic.authenticateCustomer(USERNAME, PASSWORD);

    logger.info("Execution completed.");
  }
}
