package com.samcarpentier.platformengineering.gitlabci;

public interface AuthenticationService {
  AuthenticationResult login(String username, String password);
}
