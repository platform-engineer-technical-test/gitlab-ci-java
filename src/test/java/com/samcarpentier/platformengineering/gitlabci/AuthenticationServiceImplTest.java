package com.samcarpentier.platformengineering.gitlabci;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AuthenticationServiceImplTest {

  private static final String USERNAME = "user";
  private static final String PASSWORD = "pass";

  private AuthenticationServiceImpl authenticationService;

  @BeforeEach
  void setUp() {
    authenticationService = new AuthenticationServiceImpl();
  }

  @Test
  void givenAnyCredentials_whenLogin_thenReturnSuccessResult() {
    // when
    AuthenticationResult authenticationResult = authenticationService.login(USERNAME, PASSWORD);

    // then
    assertEquals(AuthenticationResult.SUCCESS, authenticationResult, "expected successful result");
  }
}