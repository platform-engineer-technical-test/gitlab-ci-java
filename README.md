# gitlab-ci-java

Dummy project to test your GitLab-CI troubleshooting skills with a Java project!

## Requirements

* Java JDK 17.0
* Gradle 7.6
* A working *nix terminal

## Setup

### Building and running the code locally

```bash
./gradlew compileJava
./gradlew run
```

### Running unit tests locally

```bash
./gradlew test
```
